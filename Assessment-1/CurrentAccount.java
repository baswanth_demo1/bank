import java.util.*;
public class CurrentAccount {
    public static void main(String args[]){
        //ReferenceType handle = new ReferenceType();
        Bank account1 = new Bank();
        Bank account2 = new Bank();

        Scanner sc=new Scanner(System.in);  

        double initialAccountBalance1 = account1.checkBalance();
        double initialAccountBalance2 = account2.checkBalance();
        System.out.printf("Initial account balance of account1 is :: %f %n ", initialAccountBalance1);
        System.out.printf("Initial account balance of account2 is :: %f %n ", initialAccountBalance2);

        int deposit1 =  sc.nextInt(); 
        account1.deposit(deposit1);
        int deposit2 =  sc.nextInt();
        account2.deposit(deposit2);

        double accountBalanceAfterDeposit1 = account1.checkBalance();
        double accountBalanceAfterDeposit2 = account2.checkBalance();

        System.out.printf("Account balance of account 1 after deposit is :: %f %n ", accountBalanceAfterDeposit1);
        System.out.printf("Account balance of account 2 after deposit is :: %f %n ", accountBalanceAfterDeposit2);

        int withdraw1 =  sc.nextInt(); 
        account1.withdraw(withdraw1);
        int withdraw2 =  sc.nextInt();
        account2.withdraw(withdraw2);

        double accountBalanceAfterWithdraw1 = account1.checkBalance();
        double accountBalanceAfterWithdraw2 = account2.checkBalance();
        System.out.printf("Account balance of account 1 after withdrawal is :: %f %n ", accountBalanceAfterWithdraw1);
        System.out.printf("Account balance of account 2 after withdrawal is :: %f %n ", accountBalanceAfterWithdraw2);
    }
}
