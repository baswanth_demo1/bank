import java.util.*;

public class Bank {

   
    private String ownerName;
    private double accountBalance;
    private Date dateofopening;
    private String status;

    public void deposit(double amount ){
        this.accountBalance = this.accountBalance + amount;
    }

    public double withdraw(double amount){
        if ( this.accountBalance - amount > 0){
            this.accountBalance = this.accountBalance - amount;
            return amount;
        }
        return 0;
    }

    public double checkBalance(){
        return this.accountBalance;
    }

    public String getCustomerName(){
        return this.ownerName;
    }

   

}
